# -*- coding: utf-8 -*-
# Для ВКонтакте
import vk_api, random, time
import datetime
from vk_api.longpoll import VkLongPoll, VkEventType
from vk_api.utils import get_random_id

# UI
from tkinter import *
from tkinter import scrolledtext
from threading import Thread


def main():
    vk_session = vk_api.VkApi(
        token=#token)

    session_api = vk_session.get_api()
    longpoll = VkLongPoll(vk_session)

    def send_message(peer_id, message=None, attachment=None, keyboard=None, payload=None):
        session_api.messages.send(peer_id=peer_id, message=message, random_id=random.randint(-2147483648, +2147483648),
                                  attachment=attachment, keyboard=keyboard, payload=payload)

    def feedback_2():
        # TODO: UI-панелька
        window = Tk()

        window.title(f"{username}")
        window.geometry('300x150')
        lbl = Label(window, font=("Arial", 10),
                    text=f'Пользователь: {username} ({event.user_id})\nчто-то пишет вам\n\n{datetime.datetime.now().strftime("%d %B")} в {datetime.datetime.now().strftime("%H:%M")}')

        lbl.pack()
        window.mainloop()

    def feedback():
        # TODO: UI-панелька
        def clicked():
            m_text = txt.get()
            window.destroy()
            window.quit()
            if m_text == '' or m_text == ' ':
                send_message(peer_id=event.user_id,
                             message='Придурашный ничего не написал.\nПодождите, в скором времени вам ответят.')
            else:
                send_message(peer_id=event.user_id, message=m_text)

        def cancel():
            window.destroy()
            window.quit()
            send_message(peer_id=event.user_id,
                         message='Это дэбил отказался вам отвечать.\nНе удаляйте сообщение, возможно в скором времени вам ответят.')

        def insf():
            m_text = f'пользователя: {username} ({event.user_id}) | Текст: {event.text}'

        window = Tk()

        window.title(f"Диалоги")
        lbl = Label(window, font=("Arial", 10), text=f"\nТекст:\n {event.text}\n")

        txt = Entry(window, width=35)
        btn = Button(window, text="Отправить", command=clicked)
        btn_cansel = Button(window, text="Отменить", command=cancel)

        lbl.grid(row=1, columnspan=2, sticky="n")
        txt.grid(row=2, columnspan=2, sticky="n")
        btn.grid(row=3, column=0, sticky="n")
        btn_cansel.grid(row=3, column=1, sticky="n")
        # st = scrolledtext.ScrolledText(window, width=70, height=10)
        # window.command(insf())
        # st.grid(column=0, row=0)
        window.mainloop()

    for event in longpoll.listen():

        if event.type == VkEventType.MESSAGE_NEW:
            user = vk_session.method("users.get", {"user_ids": event.user_id})
            username = user[0]['first_name'] + ' ' + user[0]['last_name']
            print('Новое сообщение ', end='')
            if event.from_me:
                print(f'для ', end='')
                if event.from_user:
                    print(f'{username} ({event.user_id}) | Текст: {event.text}')
                elif event.from_chat:
                    print(f'беседы с IDc: {event.chat_id} | Текст: {event.text}')
                elif event.from_group:
                    print(f'группы с IDg: {-event.group_id} | Текст: {event.text}')

            elif event.to_me:
                print(f'от ', end='')
                if event.from_user:
                    print(f'пользователя: {username} ({event.user_id}) | Текст: {event.text}')
                    # tele_main(text=f'Пользователь с IDu: {event.user_id} написал вам: {event.text}\nОтветить ему?')
                    feedback()
                elif event.from_chat:
                    print(
                        f'пользователя: {username} ({event.user_id}) с беседы с IDc: {event.chat_id} | Текст: {event.text}')
                    # send_message(peer_id=int(event.chat_id)+2000000000, message='Ну привет. © bot')
                elif event.from_group:
                    print(f'группы с IDg: {-event.group_id} | Текст: {event.text}')
                    # send_message(peer_id=-event.group_id, message='Ну привет. © bot')

            print(end='-------------------------\n')

        elif event.type == VkEventType.USER_TYPING:
            user = vk_session.method("users.get", {"user_ids": event.user_id})
            username = user[0]['first_name'] + ' ' + user[0]['last_name']
            print(
                f'Пользователь: {username} ({event.user_id}) что-то пишет вам | {datetime.datetime.now().strftime("%d %B")} в {datetime.datetime.now().strftime("%H:%M")}',
                end='\n-------------------------\n')
            feedback_2()
            # tele_main(text=f'Пользователь с IDu: {event.user_id} что-то пишет вам')

            # send_message(peer_id=event.user_id, message='Не пугайся, но я вижу, как ты пишешь. © bot')
            # return main()

        elif event.type == VkEventType.USER_TYPING_IN_CHAT:
            print(
                f'Пользователь с IDu {event.user_id}, в беседе: {event.chat_id}, что-то пишет. | {datetime.datetime.now().strftime("%d %B")} в {datetime.datetime.now().strftime("%H:%M")}',
                end='\n-------------------------\n')


if __name__ == '__main__':
    print('Запущено')
    main()
